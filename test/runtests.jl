using IonimagingImageTypes
using Test

function test_basic_operations(a, b)
    @test a .+ b == [5 5;5 5]
    @test a .+ zeros(Int, 2, 2) == a.arr
    @test a .+ 0 == a.arr
    @test a + 0 == a.arr

    @test a .- b == [0 0;1 3]
    @test a .- zeros(Int, 2, 2) == a.arr
    @test a .- 0 == a.arr
    @test a - 0 == a.arr

    @test a .* b == [4 6;6 4]
    @test a .* ones(Int, 2, 2) == a.arr
    @test a .* 1 == a.arr
    @test a * 1 == a.arr

    @test a ./ b == [1/4 2/3;3/2 4/1]
    @test a ./ ones(Int, 2, 2) == a.arr
    @test a ./ 1 == a.arr
    @test a / 1 == a.arr

    @test a .÷ b == [0 0;1 4]
    @test a .÷ ones(Int, 2, 2) == a.arr
    @test a .÷ 1 == a.arr
    @test a ÷ 1 == a.arr

    @test size(a) == size(a.arr)
    @test a[1, 1] == a.arr[1, 1]
    @test (a[1, 1] = 1) == 1
    @test length(a) == length(a.arr)
    @test size(similar(a).arr) == size(similar(a.arr))
end

@testset "IonimagingImageTypes.jl" begin
    @testset "Ionimage type" begin
        @test Ionimage <: AbstractIonimage

        a = Ionimage([1 2;3 4])
        b = Ionimage([4 3;2 1])
        test_basic_operations(a, b)
    end


    @testset "Rawimage type" begin
        @test Rawimage <: AbstractIonimage

        a = Rawimage([1 2;3 4])
        b = Rawimage([4 3;2 1])
        test_basic_operations(a, b)
    end


    @testset "Ionspots type" begin
        @test Ionspots <: AbstractIonimage

        spots_a = [(1.0, 1.0), (1.0, 2.0), (1.0, 2.0), (2.0, 1.0), (2.0, 1.0), (2.0, 1.0), (2.0, 2.0), (2.0, 2.0), (2.0, 2.0), (2.0, 2.0)]
        spots_b = [(1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 1.0), (1.0, 2.0), (1.0, 2.0), (1.0, 2.0), (2.0, 1.0), (2.0, 1.0), (2.0, 2.0)]
        a = Ionspots(spots_a, 2, 2)
        b = Ionspots(spots_b, 2, 2)
        test_basic_operations(a, b)
    end

    @testset "Unit tests" begin
        # convert a coordinate in 3×3 to that in 9×9
        @test magnify((1, 1), 3) === (2, 2)
        @test magnify((2, 1), 3) === (5, 2)
        @test magnify((3, 1), 3) === (8, 2)
        @test magnify((1, 2), 3) === (2, 5)
        @test magnify((2, 2), 3) === (5, 5)
        @test magnify((3, 2), 3) === (8, 5)
        @test magnify((1, 3), 3) === (2, 8)
        @test magnify((2, 3), 3) === (5, 8)
        @test magnify((3, 3), 3) === (8, 8)
        @test magnify((0.83, 0.83), 3) === (1, 1)
        @test magnify((0.84, 0.84), 3) === (2, 2)
        @test magnify((1.16, 1.16), 3) === (2, 2)
        @test magnify((1.17, 1.17), 3) === (3, 3)

        # convert a coordinate in 3×3 to that in 6×6
        @test magnify((1, 1), 2) === (2, 2)
        @test magnify((2, 1), 2) === (4, 2)
        @test magnify((3, 1), 2) === (6, 2)
        @test magnify((1, 2), 2) === (2, 4)
        @test magnify((2, 2), 2) === (4, 4)
        @test magnify((3, 2), 2) === (6, 4)
        @test magnify((1, 3), 2) === (2, 6)
        @test magnify((2, 3), 2) === (4, 6)
        @test magnify((3, 3), 2) === (6, 6)
        @test magnify((0.9, 0.9), 2) === (1, 1)
        @test magnify((1.4, 1.4), 2) === (2, 2)
        @test magnify((1.5, 1.5), 2) === (3, 3)
    end
end
